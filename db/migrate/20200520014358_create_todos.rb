class CreateTodos < ActiveRecord::Migration[6.0]
  def change
    create_table :todos, primary: :"id", id: :bigint, index: true, auto_incruement: true do |t|
      t.string :title, limit: 100, null: false
      t.string :detail, limit: 1000
      t.datetime :date
      t.timestamps null: true
    end
  end
end
