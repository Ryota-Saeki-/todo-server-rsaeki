require 'rails_helper'

RSpec.describe "Todos", type: :request do
  let!(:todos) { create_list(:todo, 10) }
  let(:todo_id) { todos.first.id }

  describe 'GET /todos' do
    context 'データがある時' do
      example '一覧取得成功' do
        get '/todos'

        expect(response.status).to eq 200
        expect(json).to be_present
        expect(json['todos'].size).to eq 10
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'サーバー側のエラー時' do
      example '一覧取得失敗' do
        allow(Todo).to receive(:select).and_raise(ActiveRecord::ActiveRecordError)

        get '/todos'

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 3
        expect(json['error_message']).to eq '一覧の取得に失敗しました'
      end
    end

    example 'その他のエラーで一覧取得失敗' do
      allow(Todo).to receive(:select).and_raise(Exception)

      get '/todos'

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end

  describe 'POST /todos' do
    let(:valid_attributes) { { title: 'hello',  detail: 'test', date: '2020-01-01' } }

    context 'データがある時' do
      example 'ToDo登録成功' do
        post '/todos', params: valid_attributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'titleの文字数が100の時' do
      let(:max_title_arttributes) { { title: 'a' * 100, detail: 'test', date: '2020-01-01' } }

      example 'ToDo登録成功' do
        post '/todos', params: max_title_arttributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'detailの文字数が1000の時' do
      let(:max_detail_arttributes) { { title: 'hello', detail: 'a' * 1000, date: '2020-01-01'} }

      example 'ToDo登録成功' do
        post '/todos', params: max_detail_arttributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'titleが空白の時' do
      let(:no_title_arttributes) { { title: '', detail: 'test', date: '2020-01-01' } }

      example 'リクエスト形式が不正' do
        post '/todos', params: no_title_arttributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'titleの文字数101の時' do
      let(:over_title_arttributes) { { title: 'a' * 101, detail: 'test', date: '2020-01-01' } }

      example 'リクエスト形式が不正' do
        post '/todos', params: over_title_arttributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'detailの文字数が1001の時' do
      let(:over_detail_arttributes) { { title: 'hello', detail: 'a' * 1001, date: '2020-01-01' } }

      example 'リクエスト形式が不正' do
        post '/todos', params: over_detail_arttributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'サーバー側のエラー時' do
      example 'ToDo登録失敗' do
        allow(Todo).to receive(:create!).and_raise(ActiveRecord::ActiveRecordError)

        post '/todos', params: valid_attributes

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 4
        expect(json['error_message']).to eq '登録に失敗しました'
      end
    end

    example 'その他のエラーで登録失敗' do
      allow(Todo).to receive(:create!).and_raise(Exception)

      post '/todos', params: valid_attributes

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end

  describe 'PUT /todos/:id' do
    let(:valid_attributes) { { title: 'hello', detail: 'test', date: '2020-06-04' } }

    context 'データがある時' do
      example '更新成功' do
        put "/todos/#{todo_id}", params: valid_attributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'titleの文字数が100の時' do
      let(:max_title_arttributes) { { title: 'b' * 100, detail: 'test', date: '2020-06-04' } }

      example 'ToDo更新成功' do
        put "/todos/#{todo_id}", params: max_title_arttributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'detailの文字数が1000の時' do
      let(:max_detail_arttributes) { { title: 'hello', detail: 'b' * 1000, date: '2020-06-04' } }

      example 'ToDo更新成功' do
        put "/todos/#{todo_id}", params: max_detail_arttributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'titleの文字数が101の時' do
      let(:over_title_arttributes) { { title: 'b' * 101, detail: 'test', date: '2020-06-04' } }

      example 'リクエスト形式が不正' do
        put "/todos/#{todo_id}", params: over_title_arttributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'detailの文字数が1001の時' do
      let(:over_detail_arttributes) { { title: 'hello', detail: 'b' * 1001, date: '2020-06-04' } }

      example 'リクエスト形式が不正' do
        put "/todos/#{todo_id}", params: over_detail_arttributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'サーバー側のエラー時' do
      example 'ToDo更新失敗' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(ActiveRecord::ActiveRecordError)

        put "/todos/#{todo_id}", params: valid_attributes

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 5
        expect(json['error_message']).to eq '更新に失敗しました'
      end
    end

    example 'その他のエラーで更新失敗' do
      allow_any_instance_of(Todo).to receive(:update!).and_raise(Exception)

      put "/todos/#{todo_id}", params: valid_attributes

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end

  describe 'DELETE/todos/:id' do

    context 'データがある時' do
      example 'ToDo削除成功' do
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'データがない時' do
      before { Todo.find(todo_id).destroy }

      example 'ToDo削除失敗' do
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'サーバー側のエラー時' do
      example 'ToDo削除失敗' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(ActiveRecord::ActiveRecordError)

        delete "/todos/#{todo_id}"
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 6
        expect(json['error_message']).to eq '削除に失敗しました'
      end
    end

    example 'その他のエラーで削除失敗' do
      allow_any_instance_of(Todo).to receive(:destroy!).and_raise(Exception)

      delete "/todos/#{todo_id}"

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end
end
